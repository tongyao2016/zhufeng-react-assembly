<!--
 * @Author: your name
 * @Date: 2020-08-22 13:41:16
 * @LastEditTime: 2020-08-22 14:09:25
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \zhufeng-react-assembly\part03\githubAction.md
-->
## github-actions????
- [github-actions????](http://www.ruanyifeng.com/blog/2019/09/getting-started-with-github-actions.html)

## 1.??token
- 1.??github??? [tokens](https://github.com/settings/tokens)??
- 2. ??token????????
### ??token??
![](http://img.zhufengpeixun.cn/1.token.png)
### ??token,???
![](http://img.zhufengpeixun.cn/2.token.png)
### ?token????
![](http://img.zhufengpeixun.cn/3.token.png)

## 2.????
?package.json?????`build-storybook`??

```diff
  "scripts": {
    "start": "react-scripts start",
    "build": "tsc -p tsconfig.build.json",
    "test": "react-scripts test",
    "eject": "react-scripts eject",
    "storybook": "start-storybook -p 6006 -s public",
+    "build-storybook": "build-storybook --no-dll --quiet",
    "coverage": "react-scripts test --coverage --watchAll=false",
    "coverall": "npm run coverage  && cat ./coverage/lcov.info | ./node_modules/coveralls/bin/coveralls.js && rm -rf ./coverage"
  },
```

## 3.??ACCESS_TOKEN
- ?[secrets](https://github.com/zhufengnodejs/zhufengreactui/settings/secrets)?????Secret
- ???`ACCESS_TOKEN`, ??????????`token`

![writesccesstoken](http://img.zhufengpeixun.cn/writesccesstoken.png)

## 4.??Actions
- ?????[actions(https://github.com/zhufengnodejs/zhufengreactui/actions)??
- ????????,?????`.github\workflows\blank.yml`???

![setupworkflow](http://img.zhufengpeixun.cn/setupworkflow.png)

```js
name: Build and Deploy
on: [push]
jobs:
  build-and-deploy:
    runs-on: ubuntu-latest
    steps:
      - name: Checkout ???
        uses: actions/checkout@v2 # If you're using actions/checkout@v2 you must set persist-credentials to false in most cases for the deployment to work correctly.
        with:
          persist-credentials: false

      - name: Install and Build ?? # This example project is built using npm and outputs the result to the 'build' folder. Replace with the commands required to build your project, or remove this step entirely if your site is pre-built.
        run: |
          npm install
          npm run build-storybook

      - name: Deploy ??
        uses: JamesIves/github-pages-deploy-action@releases/v3
        with:
          GITHUB_TOKEN: ${{secrets.ACCESS_TOKEN}}
          BRANCH: gh-pages # The branch the action should deploy to.
          FOLDER: storybook-static # The folder the action should deploy. 
```

## 5.??Action??
- ????`git pull` ????????`blank.yml`??????

## 6.?????
- ?[??](https://github.com/zhufengnodejs/zhufengreactui/actions)??????????

![buildsuccess](http://img.zhufengpeixun.cn/buildsuccess.png)